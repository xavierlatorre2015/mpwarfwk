<?php

namespace Mpwarfwk\Component\Routing;

class Route
{
    const DEFAULT_ACTION = 'index';

    public $name;
    public $controller;
    public $action;

    public function __construct($name, $controller, $action)
    {
        $this->name       = $name;
        $this->controller = $controller;
        $this->action     = $action;
    }

    public static function simpleRegister($name, $controller)
    {
        return new self($name, $controller, self::DEFAULT_ACTION);
    }

    public static function register($name, $controller, $action)
    {
        return new self($name, $controller, $action);
    }

    public function getController()
    {
        return $this->controller;
    }

    public function getAction()
    {
        return $this->action;
    }
}
