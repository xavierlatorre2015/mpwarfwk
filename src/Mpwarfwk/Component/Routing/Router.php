<?php

namespace Mpwarfwk\Component\Routing;

use Exception;
use Mpwarfwk\Component\Request;
use Symfony\Component\Yaml\Parser;

class Router
{
    private $routes;

    public function __construct(Parser $ymlParser, $pathRouteFile)
    {
        $this->routes = [];
        $this->setRoutingDirectory($ymlParser, $pathRouteFile);
    }

    private function setRoutingDirectory(Parser $ymlParser, $pathRouteFile)
    {
        try {
            $routeFileContent = $ymlParser->parse(file_get_contents($pathRouteFile));

            foreach ($routeFileContent as $key => $url)
            {
                if (array_key_exists(1, $url)) {
                    $this->routes[$key] = Route::register($key, $url[0], $url[1]);
                } else {
                    $this->routes[$key] = Route::simpleRegister($key, $url[0]);
                }
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getRoute(Request $request)
    {
        $controller = $request->setParams(1);
        return $this->routes[$controller];
    }
}
