<?php

namespace Mpwarfwk\Component;

class Request
{
    private $get;
    private $post;
    private $cookie;
    private $session;
    private $server;
    private $params;

    public function __construct($get, $post, $cookie, $session, $server)
    {
        $this->get = $get;
        $this->post = $post;
        $this->cookie = $cookie;
        $this->session = $session;
        $this->server = $server;
    }

    public static function create()
    {
        session_start();
        return new self($_GET,$_POST,$_COOKIE,$_SESSION, $_SERVER);
    }

    public function setParams($params)
    {
        $url = explode("/", parse_url($this->server['REQUEST_URI'])['path']);
        if (array_key_exists($params, $url)) {
            return $url[$params];
        }

        return 'index';
    }
}
