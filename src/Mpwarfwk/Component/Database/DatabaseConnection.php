<?php

namespace Mpwarfwk\Component\Database;

interface DatabaseConnection
{
    public function execute($query,$arrayOfParams);
}
