<?php

namespace Mpwarfwk\Component\Database;

use Exception;
use PDO;

class PDOConnection implements DatabaseConnection
{
    protected $pdo = null;

    public function __construct(PDO $pdo)
    {
        try{
            $this->pdo = $pdo;
        } catch(Exception $e) {
            die('Error: '.$e->getMessage());
        }
    }

    public function execute($query, $params)
    {
        $stmt = $this->pdo->prepare($query);
        $stmt->execute($params);
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $rows;
    }
}
