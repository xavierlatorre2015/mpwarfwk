<?php

namespace Mpwarfwk\Component\Template;

use Mpwarfwk\Component\Template\Template;
use Twig_Environment;

class TwigTemplate implements Template
{
    private $twig;
    private $variables;

    public function __construct(Twig_Environment $twig)
    {
        $this->twig = $twig;
        $this->variables = [];
    }

    public function assignVar($var, $value)
    {
        $this->variables[$var] = $value;
    }

    public function createView($template, $values = [])
    {
        foreach($this->variables as $key => $variableVal) {
            $values[$key] = $variableVal;
        }

        return $this->twig->render($template, $values);
    }
}
