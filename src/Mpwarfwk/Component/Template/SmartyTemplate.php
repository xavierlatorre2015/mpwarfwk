<?php

namespace Mpwarfwk\Component\Template;

use Mpwarfwk\Component\Template\Template;
use Smarty;

class SmartyTemplate implements Template
{
    private $smarty;

    public function __construct(Smarty $smarty)
    {
        $this->smarty = $smarty;
        $this->smarty->setTemplateDir(ROOTPATH . '/web/smarty');
        $this->smarty->setCompileDir(ROOTPATH . '/web/smarty/templates_c');
        $this->smarty->setCacheDir(ROOTPATH . '/web/smarty/cache');
    }

    public function assignVar($var, $value)
    {
        $this->smarty->assign($var, $value);
    }

    public function createView($template, $values = [])
    {
        foreach ($values as $key => $value) {
            $this->smarty->assign($key, $value);
        }

        return $this->smarty->display($template);
    }
}
