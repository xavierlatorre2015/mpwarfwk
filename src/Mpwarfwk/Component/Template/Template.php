<?php

namespace Mpwarfwk\Component\Template;

interface Template
{
	public function assignVar($variable, $value);
	public function createView($templateName, $values);
}
