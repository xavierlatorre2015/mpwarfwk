<?php
namespace Mpwarfwk\Component\Response;

class ResponseJson implements ResponseI
{
    private $response;

    public function __construct($result)
    {
        $this->response = json_encode($result);
    }

    public function send()
    {
        header('Content-Type: application/json');
        echo $this->response;
    }
}
