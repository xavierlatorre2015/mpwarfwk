<?php
namespace Mpwarfwk\Component\Response;

class ResponseHttp implements ResponseI
{
    public function __construct($result)
    {
        $this->response = <<<EOT
       <html>
       <head>
       </head>
       <body>
                $result
       </body>
       </html>
EOT;
    }

    public function send()
    {
        header('Content-Type: text/html; charset=UTF-8');
        echo $this->response;
    }
}
