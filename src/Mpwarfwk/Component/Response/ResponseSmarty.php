<?php
namespace Mpwarfwk\Component\Response;

class ResponseSmarty implements ResponseI
{
    public $response;

    public function __construct($result)
    {
        $this->response = $result;
    }

    public function send()
    {
        header('Content-Type: text/html; charset=UTF-8');
        echo $this->response;
    }
}
