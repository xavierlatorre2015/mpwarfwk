<?php

namespace Mpwarfwk\Component\Container;

use Exception;
use ReflectionClass;
use Symfony\Component\Yaml\Parser;

class Container
{
	private $container;
	private $services;
	private $serviceOpts;

	public function __construct(Parser $ymlParser, $configYmlPath, $environment = 'DEV')
	{
		$this->container   = [];
		$this->services    = [];
		$this->serviceOpts = [];

		$prod = $ymlParser->parse(file_get_contents($configYmlPath . '/services.yml'));

		foreach ($prod as $service => $content) {
			if (array_key_exists($service, $this->container)) {
				continue;
			}

			$this->container[$service] = $content;
			$this->serviceOpts[$service]['public'] = $content['public'];

			if(isset($content['tags'])) {
				foreach($content['tags'] as $tag) {
					$this->serviceOpts[$service]['tags'][] = $tag;
				}
			}
		}

		if($environment === 'DEV') {
			$developmentServices = $ymlParser->parse(file_get_contents($configYmlPath . '/services_dev.yml'));
			foreach($developmentServices as $service => $content) {
				if (array_key_exists($service, $this->container)) {
					continue;
				}

				$this->container[$service] = $content;
				$this->serviceOpts[$service]['public'] = $content['public'];

				if(isset($content['tags'])) {
					foreach($content['tags'] as $tag) {
						$this->serviceOpts[$service]['tags'][] = $tag;
					}
				}
			}
		}
	}

	public function getService($serviceName)
	{
		if (!array_key_exists($serviceName, $this->container)) {
			throw new Exception('Service not found: ' . $serviceName);
		}

		$this->services[$serviceName] = $this->createService($this->container[$serviceName]);
		if(!$this->serviceOpts[$serviceName]['public']) {
			throw new Exception('The Service cannot be provided por public access.');
		}
		return $this->services[$serviceName];
	}

	private function createService($serviceSchema)
	{
		if (isset($serviceSchema['arguments'])) {
			$servicesArgs = [];
			foreach ($serviceSchema['arguments'] as $argument) {
				$firstChar = substr($argument, 0, 1);

				if ('@' === $firstChar) {
					$actService = str_replace("@", "", $argument);
					$servicesArgs[] = $this->createService($this->container[$actService]);
				} else {
					$firstChar = substr($argument, 0, 1);
					if ('[' === $firstChar) {
						$cleanArgs = str_replace("[", "", $argument);
						$cleanArgs = str_replace("]", "", $cleanArgs);
						$cleanArgs = str_replace("'", "", $cleanArgs);
						$cleanArgs = str_replace(" ", "", $cleanArgs);
						$cleanArgs = explode(",", $cleanArgs);
						$argument = [];

						foreach ($cleanArgs as $cleanArg) {
							$arrVals = explode("=>", $cleanArg);
							$argument[$arrVals[0]] = $arrVals[1];
						}
					}
					$servicesArgs[] = $argument;
				}
			}
			$reflector = new ReflectionClass($serviceSchema['class']);
			return $reflector->newInstanceArgs($servicesArgs);
		}
		return new $serviceSchema['class']();
	}
}
