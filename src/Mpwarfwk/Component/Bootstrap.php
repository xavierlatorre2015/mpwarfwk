<?php
namespace Mpwarfwk\Component;

use Mpwarfwk\Component\Container\Container;
use Mpwarfwk\Component\Routing\Router;

class Bootstrap
{
    private $router;
    private $container;

    public function __construct(Router $router)
    {
        date_default_timezone_set('Europe/Paris');
        $this->router = $router;
    }

    public function setContainer(Container $container)
    {
        $this->container = $container;
    }

    public function execute(Request $request)
    {
        $route      = $this->router->getRoute($request);
        $controller = $route->getController();
        $action     = $route->getAction();

        $controller_instance = new $controller($this->container);

        return $controller_instance->$action();
    }
}
