<?php

namespace Mpwarfwk\Component\Controller;

use Mpwarfwk\Component\Container\Container;

abstract class ControllerBase
{
	protected $container;

	public function __construct(Container $container)
	{
		$this->container = $container;
	}
}